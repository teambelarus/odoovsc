FROM odoo:15.0
LABEL Name=odoovsc Version=0.0.1
USER root
RUN pip3 install ptvsd && cp /usr/bin/odoo /usr/bin/odoo-bin
COPY odoo-bin /usr/bin/odoo
EXPOSE 5678
USER odoo
